/// <reference types="Cypress" />
import LoginPage from "./pageObject/LoginPage";
import { fill } from "../../node_modules/cypress/types/lodash/index";
const testData = require("../fixtures/testData");

describe("Test suite for login ", function () {
    let lp: LoginPage;

    testData.forEach((testDataRow: any) => {
        const data = {
            term: testDataRow.term,
            password_result: testDataRow.password_result,
            expected_result: testDataRow.expected_result,
        };

        beforeEach(() => {
            lp = new LoginPage();
        });

        context(`Generating a test for ${data.term} and ${data.password_result} and ${data.expected_result}`, () => {
            it("test login for the specified details", () => {
                lp.visit();
                lp.fillEmail(data.term);
                lp.fillPassword(data.password_result);
                if ((data.term == " ") || (data.password_result  ==" ")|| (data.expected_result  ==" ")  ) {
                 lp.error();
                }
                  else {
                
                    lp.validateEmail(data.term);
                    lp.validatePassword(data.password_result);
                    lp.submit();

                    if (data.expected_result == "success") {
                        lp.islogin();
                        lp.success();
                        lp.logout();
                       lp.successlogout();
                    } else {
                    lp.faill(data.password_result);
                    }
                }
             });
        });
    });
});
