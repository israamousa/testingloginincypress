/// <reference types="Cypress" />

class LoginPage {
    visit() {
        cy.visit("https://admin-demo.nopcommerce.com/login");
    }

    fillEmail(term: String) {
        // to fill email here
        const field = cy.get("[id=Email]"); //css selector
        field.clear();
        field.type(`${term}`);
        return this;
    }

    fillPassword(password_result: String) {
        // to fill email here
        const field = cy.get("[id=Password]"); //css selector
        field.clear();
        field.type(`${password_result}`);
        return this;
    }

    validateEmail(term: String) {
        cy.get("[id=Email]").should("have.value", `${term}`);
    }

    validatePassword(term: String) {
        cy.get("[id=Password").should("have.value", `${term}`);
    }
    error() {
        cy.log("...error you try to enter empty data pleaze enter again....");
    }

    success() {
        cy.log("success log in");
    }
    successlogout() {
        cy.log("success logout you can login again");
    }
    faill(term: String) {
        cy.log("............." + term + " to login " + "you can login again...............");
    }
    submit() {
        const Button = cy.get("[type=submit]");
        Button.click();
    }

    isRemmberMe() {
        const check = cy.get("[id=RememberMe]").should("be.checked");
    }

    islogin() {
        cy.title().should("be.equal", "Dashboard / nopCommerce administration");
    }

    logout() {
        const Button = cy.get('.main-header a[href*="logout"]');
        Button.click();
        cy.title().should("be.equal", "Your store. Login");
    }
}
export default LoginPage;
